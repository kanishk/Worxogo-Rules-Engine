/*
This file is part of Worxogo Rule Engine Wrapper(wre) which is a java webapp.
Copyright © 2016 Worxogo Solutions Pvt Ltd

wre is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

wre is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; If not, see <http://www.gnu.org/licenses/>. 
 */
package TATLToS;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.openrules.ruleengine.Decision;

/**
 * Servlet implementation class DecisionTATLToS
 */
@WebServlet("/DecisionTATLToS")
public class DecisionTATLToS extends HttpServlet {
	private static final long serialVersionUID = 1L;	
	public static String config_path = "/WEB-INF/properties";
	public static String config_file = "config.properties";
	private static Logger logger=Logger.getLogger("gSales");   
	public static String log4j_file = "log4j.properties";
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DecisionTATLToS() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		// TODO Auto-generated method stub
		FileInputStream input;
		try{			
			Properties prop = new Properties();
			String cfgPath = req.getServletContext().getRealPath(config_path);
			input = new FileInputStream(cfgPath+File.separator+config_file);
			prop.load(input);
			String log4jConfigFile = cfgPath+File.separator+log4j_file;
			PropertyConfigurator.configure(log4jConfigFile);				
			String path = prop.getProperty("PATH");
			logger.debug("path "+path);
			
			logger.debug("cfgPath "+cfgPath);
			
					
			String file = prop.getProperty("TATLTOS_FILE");		
			
			
			String fileName = "file:"+req.getServletContext().getRealPath(path) +File.separator+ file;
			Decision decision = new Decision("Main",fileName);			
			
			double tatltos = Double.parseDouble(req.getParameter("tatltos"));
			String caseDisbursed =req.getParameter("caseDisbursed");
			int points = executeDecision(tatltos, caseDisbursed, decision);
			res.setHeader("Points", ""+points);				
		}
		catch(Exception e){
			e.printStackTrace();			
		}
	}
	
	public int executeDecision(double tatltos, String caseDisbursed, Decision decision){		
		  SCE sce = new SCE();
		  sce.setTatLtoS(tatltos);
		  logger.debug("sce.getTatLtoS() "+sce.getTatLtoS());
		  sce.setCaseDisbursed(caseDisbursed);
		  decision.put("sce", sce);
		  Points points = new Points();
		  decision.put("points",points);		  
		  decision.saveRunLog(true);
		  decision.execute();
		  logger.debug("TAT L To S Points "+points.getTatLtoSPoints());
		  return points.getTatLtoSPoints();			 
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
