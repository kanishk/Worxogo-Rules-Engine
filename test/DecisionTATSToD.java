/*
This file is part of Worxogo Rule Engine Wrapper(wre) which is a java webapp.
Copyright © 2016 Worxogo Solutions Pvt Ltd

wre is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

wre is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; If not, see <http://www.gnu.org/licenses/>. 
 */
package test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.couchbase.client.java.Bucket;
import com.couchbase.client.java.document.json.JsonArray;
import com.couchbase.client.java.view.ViewResult;
import com.couchbase.client.java.view.ViewRow;
import com.openrules.ruleengine.Decision;

/**
 * Servlet implementation class DecisionTATSToD
 */
@WebServlet("/DecisionTATSToD")
public class DecisionTATSToD extends HttpServlet {
	private static final long serialVersionUID = 1L;	 
	private static Logger logger=Logger.getLogger("gSales");
	public static String config_path = "/WEB-INF/properties";
	public static String config_file = "config.properties";
	public static String log4j_file = "log4j.properties";
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DecisionTATSToD() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		// TODO Auto-generated method stub          
		FileInputStream input;
		try{			
			Properties prop = new Properties();
			String cfgPath = req.getServletContext().getRealPath(config_path);
			input = new FileInputStream(cfgPath+File.separator+config_file);
			prop.load(input);
			String log4jConfigFile = cfgPath+File.separator+log4j_file;
			PropertyConfigurator.configure(log4jConfigFile);
			logger.debug("in doGet");			
			
			String path = prop.getProperty("PATH");
			logger.debug("path "+path);				
					
			String file = prop.getProperty("FILE");		
			
			
			String fileName = "file:"+req.getServletContext().getRealPath(path) +File.separator+ file;
			Decision decision = new Decision("Main",fileName);				
					
			int points = executeDecision(req.getParameter("TATStoD"), decision);
			res.setHeader("Points", ""+points);				
		}
		catch(Exception e){
			e.printStackTrace();			
		}
	}
	
	public int executeDecision(String value, Decision decision){		
			  SCE sce = new SCE();			  
			  sce.setTatStoD(Double.parseDouble(value));
			  decision.put("sce", sce);
			  Points points = new Points();
			  decision.put("points",points);		  
			  decision.saveRunLog(true);
			  decision.execute();
			  return points.getTatStoDPoints();			 
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
