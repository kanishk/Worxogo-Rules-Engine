/*
This file is part of Worxogo Rule Engine Wrapper(wre) which is a java webapp.
Copyright © 2016 Worxogo Solutions Pvt Ltd

wre is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

wre is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; If not, see <http://www.gnu.org/licenses/>. 
 */
package SalesLeaderboard;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;



import org.apache.log4j.PropertyConfigurator;

import com.couchbase.client.java.document.json.JsonArray;
import com.couchbase.client.java.document.json.JsonObject;
import com.openrules.ruleengine.Decision;

@WebServlet("/DecisionSalesLeaderboard")
public class DecisionSalesLeaderboard extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public static String config_path = "/WEB-INF/properties";
	public static String config_file = "config.properties";
	private static Logger logger=Logger.getLogger("gSales");   
	public static String log4j_file = "log4j.properties";
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DecisionSalesLeaderboard() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		// TODO Auto-generated method stub
		FileInputStream input;
		try{			
			Properties prop = new Properties();
			String cfgPath = req.getServletContext().getRealPath(config_path);
			input = new FileInputStream(cfgPath+File.separator+config_file);
			prop.load(input);	
			String log4jConfigFile = cfgPath+File.separator+log4j_file;
			PropertyConfigurator.configure(log4jConfigFile);
			String path = prop.getProperty("PATH");
			logger.debug("path "+path);
			
			logger.debug("cfgPath "+cfgPath);
			
					
			String file = prop.getProperty("SALES_LEADERBOARD_FILE");		
			
			
			String fileName = "file:"+req.getServletContext().getRealPath(path) +File.separator+ file;
			Decision decision = new Decision("Main",fileName);			
			
			logger.debug("REQ parameter "+req.getParameter("scoreArr"));	
			JsonArray scoreArr = JsonArray.fromJson(req.getParameter("scoreArr"));
			JsonObject scoreObj = null;
			
			SCE sce  = new SCE();
			if(!(scoreArr.isEmpty())){								  
				  for(Object obj:scoreArr){
					  logger.debug("OBJ "+obj.toString());
					  scoreObj = (JsonObject)obj;								  
					  if(scoreObj.getString("name").equals("enquiry_points"))
						  if(scoreObj.getInt("value")!=null){
							  sce.setEnquiryPoints(scoreObj.getInt("value"));							  									  
						  }
					  
					  if(scoreObj.getString("name").equals("pddc_points"))
						  if(scoreObj.getInt("value")!=null){
							  sce.setPddcPoints(scoreObj.getInt("value"));										  
						  }
					  if(scoreObj.getString("name").equals("login_points"))
						  if(scoreObj.getInt("value")!=null){							  	
							  sce.setLoginPoints(scoreObj.getInt("value"));
						  }
					  if(scoreObj.getString("name").equals("week_of_month_points"))
						  if(scoreObj.getInt("value")!=null){							  	
							  sce.setWeekOfMonthPoints(scoreObj.getInt("value"));
						  }
					  if(scoreObj.getString("name").equals("d_ftr"))
						  if(scoreObj.getInt("value")!=null){							  
							  sce.setFtrAvailabilityPoints(scoreObj.getInt("value"));
						  }
					  if(scoreObj.getString("name").equals("case_disbursed_score"))
						  if(scoreObj.getInt("value")!=null){							  	
							  sce.setCaseDisbursedPoints(scoreObj.getInt("value"));
						  }
					  if(scoreObj.getString("name").equals("dtat_l_to_s"))
						  if(scoreObj.getInt("value")!=null){							  	
							  sce.setTatLtoSPoints(scoreObj.getInt("value"));
						  }
					  if(scoreObj.getString("name").equals("dtat_s_to_d"))
						  if(scoreObj.getInt("value")!=null){							  
							  sce.setTatStoDPoints(scoreObj.getInt("value"));
						  }
					  if(scoreObj.getString("name").equals("beat_plan_points"))
						  if(scoreObj.getInt("value")!=null){							  	
							  sce.setBeatPlanPoints(scoreObj.getInt("value"));
						  }
					  
				  }						  
				  
		    }			
			
			Points points = executeDecision(sce, decision);
			res.setHeader("SalesActivityPoints", ""+points.getSalesActivityPoints());		
			res.setHeader("SalesOutcomePoints", ""+points.getSalesOutcomePoints());
			res.setHeader("TATPoints", ""+points.getTatPoints());
			res.setHeader("SalesLeaderboardPoints", ""+points.getSalesLeaderboardPoints());			
			
		}
		catch(Exception e){
			e.printStackTrace();			
		}
	}
	
	public Points executeDecision(SCE sce, Decision decision){	
		  decision.put("sce", sce);
		  Points points = new Points();
		  decision.put("points",points);		  
		  decision.saveRunLog(true);
		  decision.execute();
		  logger.debug("Sales Activity Points "+points.getSalesActivityPoints());
		  logger.debug("Sales Leaderboard Points "+points.getSalesLeaderboardPoints());
		  logger.debug("Sales Outcome Points "+points.getSalesOutcomePoints());
		  logger.debug("TAT Points "+points.getTatPoints());
		  return points;			 
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
	}

}
