/*
This file is part of Worxogo Rule Engine Wrapper(wre) which is a java webapp.
Copyright © 2016 Worxogo Solutions Pvt Ltd

wre is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

wre is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; If not, see <http://www.gnu.org/licenses/>. 
 */
package SalesLeaderboard;

public class Points {
	double salesActivityPoints;
	double salesOutcomePoints;
	double tatPoints;
	double salesLeaderboardPoints;
	public double getSalesActivityPoints() {
		return salesActivityPoints;
	}
	public void setSalesActivityPoints(double salesActivityPoints) {
		this.salesActivityPoints = salesActivityPoints;
	}
	public double getSalesOutcomePoints() {
		return salesOutcomePoints;
	}
	public void setSalesOutcomePoints(double salesOutcomePoints) {
		this.salesOutcomePoints = salesOutcomePoints;
	}
	public double getTatPoints() {
		return tatPoints;
	}
	public void setTatPoints(double tatPoints) {
		this.tatPoints = tatPoints;
	}
	public double getSalesLeaderboardPoints() {
		return salesLeaderboardPoints;
	}
	public void setSalesLeaderboardPoints(double salesLeaderboardPoints) {
		this.salesLeaderboardPoints = salesLeaderboardPoints;
	}
	
}
