/*
This file is part of Worxogo Rule Engine Wrapper(wre) which is a java webapp.
Copyright © 2016 Worxogo Solutions Pvt Ltd

wre is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

wre is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; If not, see <http://www.gnu.org/licenses/>. 
 */
package com.worxogo.ruleengine;

import java.io.File;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openl.types.impl.DynamicObject;

import com.openrules.ruleengine.BusinessMap;
import com.openrules.ruleengine.Decision;
import com.openrules.ruleengine.DecisionObject;
import com.worxogo.model.IRuleEngineRequest;
import com.worxogo.model.IRuleEngineResponse;
import com.worxogo.model.OpenRuleEngineResponse;
import com.worxogo.utils.ApplicationUtil;
import com.worxogo.utils.RuleEngineConstant;
import com.worxogo.utils.RuntimeWrapper;

/**
 * @author anuj
 *
 */
public class OpenRulesTaskRunner {

	private static Logger _logger = LogManager
			.getLogger(OpenRulesTaskRunner.class);
	/**
	 * 
	 */
	private static String classPathURL = RuntimeWrapper.class
			.getProtectionDomain().getCodeSource().getLocation().getPath();
	private final static String ROOT_PACKAGE_NAME = "openrules.generated_class.root_package_name";
	private final static String ROOT_PATH = "openrules.generated_class.path";
	private final static String DECISION_XLS_ROOT_PATH = "openrules.decision_xls.path";
	private RuntimeWrapper runtimeWrapper;
	private Map<String, Decision> inMemoryDecisionMap = new HashMap<String, Decision>();

	/**
	 * @throws MalformedURLException
	 * 
	 */
	public OpenRulesTaskRunner() {
		runtimeWrapper = new RuntimeWrapper();
	}

	public IRuleEngineResponse executeTask(IRuleEngineRequest engineRequest) {
		_logger.debug("executeTask with engineRequest::" + engineRequest);
		Decision decision = null;
		String clientName = engineRequest.getClientName();
		String requestName = engineRequest.getRequestName();
		Map<String, String> requestParams = engineRequest.getRequestParams();
		String packageName = ApplicationUtil.getValue(ROOT_PACKAGE_NAME)
				+ (StringUtils.isNotEmpty(clientName) ? RuleEngineConstant.STRING_DOT
						+ clientName
						: "") + RuleEngineConstant.STRING_DOT + requestName;

		String folderPath = classPathURL
				+ ApplicationUtil.getValue(ROOT_PATH)
				+ (StringUtils.isNotEmpty(clientName) ? clientName
						+ RuleEngineConstant.FORWARD_SLASH : "") + requestName
				+ RuleEngineConstant.FORWARD_SLASH;

		String xlsFolderPath = ApplicationUtil.getValue(DECISION_XLS_ROOT_PATH)
				+ (StringUtils.isNotEmpty(clientName) ? clientName
						+ RuleEngineConstant.FORWARD_SLASH : "");

		// String xlsFilePath = ApplicationUtil.getValue(DECISION_XLS_ROOT_PATH)
		// + (StringUtils.isNotEmpty(clientName) ? clientName
		// + RuleEngineConstant.FORWARD_SLASH : "") + requestName
		// + ".xls";
		File decisionFile = null;
		File tempFolder = new File(xlsFolderPath);
		if (tempFolder.isDirectory()) {
			for (File file : tempFolder.listFiles()) {
				if (file.getName().contains(requestName + ".xls")
						|| file.getName().contains(requestName + ".xlsx")) {
					decisionFile = file;
					break;
				}
			}
		}
		if (null == decisionFile)
			throw new RuntimeException(
					"Decision xls does not exits for clientName=" + clientName
							+ ", requestName=" + requestName);
		if (null == inMemoryDecisionMap.get(requestName)) {
			decision = new Decision("Main", "file:"
					+ decisionFile.getAbsolutePath());
			inMemoryDecisionMap.put(requestName, decision);
		}
		decision = inMemoryDecisionMap.get(requestName);

		_logger.debug("Generating decision objects with packageName:"
				+ packageName + ",folderPath=" + folderPath + "for clientName="
				+ clientName + ",requestName=" + requestName);

		if (generateDecisionObjects(decision.getGlossary()
				.getBusinessConcepts(), packageName)) {
			decision.generateDecisionObjects(packageName, folderPath);

			try {
				runtimeWrapper.generateClass(clientName
						+ RuleEngineConstant.FORWARD_SLASH + requestName);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		List<BusinessMap> businessMaps = decision
				.createBusinessMaps(packageName);
		BusinessMap responseBusinessMap = null;
		if (requestParams.isEmpty())
			throw new RuntimeException("No Request Parameter was provided."
					+ requestParams);
		_logger.debug("requestParams=" + requestParams);
		for (BusinessMap businessMap : businessMaps) {
			Set<String> attributes = businessMap.getDecisionObject()
					.getAttributes().keySet();
			if (attributes.containsAll(requestParams.keySet())) {
				for (String attribute : attributes) {

					try {
						if (null != requestParams.get(attribute)) {
							businessMap.setAttribute(attribute,
									requestParams.get(attribute));
						}
					} catch (IllegalArgumentException ie) {
						throw new RuntimeException("Invalid Argument : "
								+ attribute + "="
								+ requestParams.get(attribute)
								+ " for business concept="
								+ businessMap.getBusinessConcept()
								+ "'s attribute=" + attribute);
					}
				}
			} else {
				responseBusinessMap = businessMap;
			}

		}

		decision.saveRunLog(true);
		decision.executeWithBusinessMaps();

		IRuleEngineResponse engineResponse = new OpenRuleEngineResponse();
		_logger.debug("responseBusinessMap = " + responseBusinessMap);
		// responseBusinessMap = null;
		Object object = decision.getBusinessObject(responseBusinessMap
				.getBusinessConcept());
		if (object instanceof DynamicObject) {
			_logger.debug("response is DynamicObject = " + object);
			DynamicObject dynamicObject = (DynamicObject) object;
			for (Object attribute : dynamicObject.getFields().keySet()) {
				Object fieldValue = dynamicObject.getFieldValue(attribute
						.toString());
				String value = fieldValue != null ? fieldValue.toString() : "";
				engineResponse.getResponseFields().put(attribute.toString(),
						value);
			}
		} else if (object instanceof DecisionObject) {
			DecisionObject responseObject = (DecisionObject) decision
					.getBusinessObject(responseBusinessMap.getBusinessConcept());
			_logger.debug("response is DecisionObject = " + object);
			for (String attribute : responseObject.getAttributes().keySet()) {
				engineResponse.getResponseFields().put(attribute,
						responseObject.getAttribute(attribute));
			}
		}

		return engineResponse;
	}

	/**
	 * See if the classes are already loaded or needs to be generated again.
	 * 
	 * @param businessConcepts
	 * @param packageName
	 * @return
	 */
	private boolean generateDecisionObjects(Set<String> businessConcepts,
			String packageName) {
		boolean generateDecisionObjects = false;
		try {
			for (String className : businessConcepts) {
				Class.forName(packageName + RuleEngineConstant.STRING_DOT
						+ className);
			}
		} catch (ClassNotFoundException e) {
			generateDecisionObjects = true;
		}
		return generateDecisionObjects;
	}

	public void rebuildTaskRunner() throws Exception {
		_logger.debug("Clearing in memory Decisions Objects");
		inMemoryDecisionMap = new HashMap<String, Decision>();

		_logger.debug("Deleting all runtime generated classes.");
		runtimeWrapper.deleteGeneratedClasses(null);
	}
}
