/*
This file is part of Worxogo Rule Engine Wrapper(wre) which is a java webapp.
Copyright © 2016 Worxogo Solutions Pvt Ltd

wre is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

wre is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; If not, see <http://www.gnu.org/licenses/>. 
 */
package com.worxogo.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author anuj
 *
 */
public class ApplicationUtil {

	private static Properties properties;
	static {
		InputStream inStream = ApplicationUtil.class.getClassLoader().getResourceAsStream("application.properties");
		properties = new Properties();
		try {
			properties.load(inStream);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static String getValue(String key) {
		return properties.getProperty(key, key);
	}

}
