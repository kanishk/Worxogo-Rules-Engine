/*
This file is part of Worxogo Rule Engine Wrapper(wre) which is a java webapp.
Copyright © 2016 Worxogo Solutions Pvt Ltd

wre is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

wre is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; If not, see <http://www.gnu.org/licenses/>. 
 */
package com.worxogo.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.worxogo.model.IRuleEngineRequest;
import com.worxogo.model.IRuleEngineResponse;
import com.worxogo.model.OpenRuleEngineRequest;
import com.worxogo.ruleengine.OpenRulesTaskRunner;

/**
 * Servlet implementation class RuleEngineServlet
 */
@WebServlet("/RuleEngineServlet")
public class RuleEngineServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger _logger = LogManager
			.getLogger(RuleEngineServlet.class);

	private OpenRulesTaskRunner openRulesTaskRunner;

	/**
	 * @throws MalformedURLException
	 * @see HttpServlet#HttpServlet()
	 */
	public RuleEngineServlet() {
		super();
		openRulesTaskRunner = new OpenRulesTaskRunner();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		if (null != request.getParameter("reload")
				&& Boolean.valueOf(request.getParameter("reload"))) {
			_logger.debug("Rebuilding Open Rules Task Runner");
			try {
				openRulesTaskRunner.rebuildTaskRunner();
				out.print("Task Runner has been reconfigured.");
				out.print("1. Cleared all in memory decision objects to create new ones on next request.");
				out.print("2. Deleted directory for runtime generated classes. New Decision Classes will be generated on next request.");
			} catch (Exception e) {
				out.print("Failed in rebuilding task runner. A re-deployment of webapp is required.");
			}
			out.flush();
		} else {
			executeRule(request, response);
		}

	}

	/**
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	private void executeRule(HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		PrintWriter out = response.getWriter();
		String clientName = "", requestName = "";
		Map<String, String> requestParams = new HashMap<String, String>();

		for (String param : request.getParameterMap().keySet()) {
			if ("clientName".equalsIgnoreCase(param)) {
				clientName = request.getParameterMap().get("clientName")[0];
			} else if ("requestName".equalsIgnoreCase(param)) {
				requestName = request.getParameterMap().get("requestName")[0];
			} else {
				requestParams.put(param,
						request.getParameterMap().get(param)[0]);
			}
		}
		IRuleEngineRequest engineRequest = new OpenRuleEngineRequest(
				clientName, requestName, requestParams);
		_logger.debug("engineRequest ::" + engineRequest);

		IRuleEngineResponse engineResponse = openRulesTaskRunner
				.executeTask(engineRequest);
		_logger.debug("engineResponse ::" + engineResponse);
		Gson gson = new Gson();
		if (null != engineResponse) {
			response.setContentType("application/json");
			String json = gson.toJson(engineResponse.getResponseFields());
			out.println(json);
			out.flush();

		} else {
			out.print(gson.toJson(new Object()));
			out.flush();
		}
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(req, resp);
	}
}
